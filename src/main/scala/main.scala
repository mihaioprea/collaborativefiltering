import model.CollaborativeFiltering

object Main {

  def main (args: Array[String]) {
    val cf = new CollaborativeFiltering()
    cf.streaming()
  }
}
