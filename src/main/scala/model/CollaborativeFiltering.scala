package model

import java.util

import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.SparkConf
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.recommendation.ALS
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Seconds, StreamingContext}

class CollaborativeFiltering extends Serializable {

  object Processor {
    case class Rating(userId: Int, movieId: Int, rating: Float, timestamp: Long)
    def parseRatings(recordList: String): Array[Rating] = {
      val lines = recordList.split("\n")
      val ratings:Array[Rating] = new Array[Rating](lines.length)
      for (i <- 0 to lines.length-1) {
        val fields = lines(1).split(",")
        assert(fields.size == 4)
        ratings(i) = ( Rating(fields(0).toInt, fields(1).toInt, fields(2).toFloat, fields(3).toLong));
      }
      return ratings
    }

    def trainModel(ratings: DataFrame)  {
      val Array(training, test) = ratings.randomSplit(Array(0.8, 0.2))

      // Build the recommendation model using ALS on the training data
      val als = new ALS()
        .setMaxIter(5)
        .setRegParam(0.01)
        .setUserCol("userId")
        .setItemCol("movieId")
        .setRatingCol("rating")

      val model = als.fit(training)

      // Evaluate the model by computing the RMSE on the test data
      // Note we set cold start strategy to 'drop' to ensure we don't get NaN evaluation metrics
      model.setColdStartStrategy("drop")
      val predictions = model.transform(test)

      val evaluator = new RegressionEvaluator()
        .setMetricName("rmse")
        .setLabelCol("rating")
        .setPredictionCol("prediction")
      val rmse = evaluator.evaluate(predictions)
      println(s"Root-mean-square error = $rmse")

      // Generate top 10 movie recommendations for each user
      val userRecs = model.recommendForAllUsers(10)
      // Generate top 10 user recommendations for each movie
      val movieRecs = model.recommendForAllItems(10)
    }
  }

  def saveModel() {
    println("Model saved")
  }

  def streaming() {
    val kafkaParams = Map[String, Object](
      "bootstrap.servers" -> "localhost:9092",
      "key.deserializer" -> classOf[StringDeserializer],
      "value.deserializer" -> classOf[StringDeserializer],
      "group.id" -> "use_a_separate_group_id_for_each_stream",
      "auto.offset.reset" -> "latest",
      "enable.auto.commit" -> (false: java.lang.Boolean)
    )

    val conf = new SparkConf().setAppName("CollabFilter1").setMaster("local[*]")
    val ssc = new StreamingContext(conf, Seconds(10))

    val topics = Array("CollaborativeFiltering")

    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, kafkaParams)
    )

    val spark = SparkSession
      .builder()
      .config(conf)
      .getOrCreate()
    import spark.implicits._
    val datasets = stream
      // each kafka message is as dataset
      .map(msg =>  {
      import Processor.parseRatings
      import Processor.Rating
      parseRatings(msg.value)
    })
      .map(ratings => {
        import Processor.trainModel
        import Processor.Rating
        trainModel(spark.createDataFrame(ratings))
      })

    datasets.foreachRDD(
      rdd => {
        rdd.foreachPartition(
          partition => {
            import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord}
            val props = new util.HashMap[String, Object]()
            props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "")
            props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
              "org.apache.kafka.common.serialization.StringSerializer")
            props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
              "org.apache.kafka.common.serialization.StringSerializer")
            val producer = new KafkaProducer[String, String](props)

            val kafkaOpTopic = "ModelStorageTopic"

            partition.foreach( record => {
              val data = record.toString

              val message = new ProducerRecord[String, String](kafkaOpTopic, null, data)
              producer.send(message)
            } )
            producer.close()
          }
        )
      }
    )

    ssc.start()
    ssc.awaitTermination()
  }
}
